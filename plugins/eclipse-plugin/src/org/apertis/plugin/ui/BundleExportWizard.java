/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin.ui;

import org.apertis.plugin.jobs.BundleExportJob;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

public class BundleExportWizard extends Wizard implements IExportWizard {
	BundleExportWizardPage page;

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		page = new BundleExportWizardPage(selection, "destPage");
		addPage(page);
	}

	@Override
	public boolean performFinish() {
		BundleExportJob job;

		job = new BundleExportJob(page.getSelectedProject(), page.getDestination());
		job.schedule();

		return true;
	}

}
