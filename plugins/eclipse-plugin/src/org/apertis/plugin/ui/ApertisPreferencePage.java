/*Copyright © 2016 Collabora Ltd.

SPDX-License-Identifier: MPL-2.0
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

package org.apertis.plugin.ui;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.apertis.plugin.ApertisPlugin;
import org.apertis.plugin.Constants;

public class ApertisPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public ApertisPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
	}

	@Override
	public void init(IWorkbench arg0) {
		setPreferenceStore(ApertisPlugin.getDefault().getPreferenceStore());
	}

	@Override
	protected void createFieldEditors() {
		StringFieldEditor usernameEditor = new StringFieldEditor(
				Constants.APERTIS_SYSROOT_UPDATE_USERNAME_PREFERENCE,
				Constants.USERNAME_LABEL,
				getFieldEditorParent());
		addField(usernameEditor);

		StringFieldEditor passwordEditor = new StringFieldEditor(
				Constants.APERTIS_SYSROOT_UPDATE_PASSWORD_PREFERENCE,
				Constants.PASSWORD_LABEL,
				getFieldEditorParent()) {
			@Override
			protected void doFillIntoGrid(Composite parent, int numColumns) {
				super.doFillIntoGrid(parent, numColumns);
				getTextControl().setEchoChar('*');
			}
		};
		addField(passwordEditor);

		BooleanFieldEditor sslEditor = new BooleanFieldEditor(
				Constants.APERTIS_SYSROOT_UPDATE_SSL_PREFERENCE,
				Constants.SSL_CHECK_LABEL,
				getFieldEditorParent());
		addField(sslEditor);

		Button updateButton = new Button(getFieldEditorParent(), SWT.PUSH | SWT.CENTER);
		updateButton.setText(Constants.CHECK_UPDATE_BUTTON_LABEL); //$NON-NLS-1$
		updateButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				performApply();
				SysrootUpdateDialog updateDialog = new SysrootUpdateDialog(getShell());
				updateDialog.create();
				updateDialog.open();
			}
		});
		GridData updateData = new GridData(GridData.FILL_HORIZONTAL);
		updateData.horizontalSpan = 2;
		updateData.grabExcessHorizontalSpace = true;
		updateButton.setLayoutData(updateData);
	}
}
