/*Copyright © 2016 Collabora Ltd.

SPDX-License-Identifier: MPL-2.0
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

package org.apertis.plugin;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.cdt.core.CommandLauncher;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.preference.IPreferenceStore;

public class SysrootManager {
	
	private static Map<String, String> runCmd(List<String> args) throws InvalidSysrootException {
		CommandLauncher launcher = new CommandLauncher();
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		ByteArrayOutputStream stderr = new ByteArrayOutputStream();

		args.add(0, "--format=parseable");
		args.add(1, "sysroot");

		try {
			launcher.execute(new Path("/usr/bin/ade"), args.toArray(new String[0]), null, null, null);
			if (launcher.waitAndRead(stdout, stderr, new NullProgressMonitor()) != CommandLauncher.OK) {
				throw new InvalidSysrootException(stderr.toString());
			}
		} catch (CoreException e) {
			throw new InvalidSysrootException(e.toString());
		}

		return AdeHelper.parseOutput(stdout.toString());
	}

	private static void addAuth(List<String> args) {
		IPreferenceStore store = ApertisPlugin.getDefault().getPreferenceStore();
		String user = store.getString(Constants.APERTIS_SYSROOT_UPDATE_USERNAME_PREFERENCE);
		String password = store.getString(Constants.APERTIS_SYSROOT_UPDATE_PASSWORD_PREFERENCE);

		if (!user.isEmpty() && !password.isEmpty()) {
			args.add("--user");
			args.add("'" + user.replaceAll("'", "'\"'\"'") + "'");
			args.add("--password");
			args.add("'" + password.replaceAll("'",  "'\"'\"'") + "'");
		}
	}
	
	public static Sysroot getInstalled() throws InvalidSysrootException {
		List<String> args = new ArrayList<String>();
		String result;

		args.add("installed");
		
		result = runCmd(args).get("InstalledVersion");
		if (result == null)
			return null;
		return new Sysroot(result);
	}

	public static Sysroot getLatest() throws InvalidSysrootException {
		List<String> args = new ArrayList<String>();
		Map<String, String> result;

		args.add("latest");

		addAuth(args);

		result = runCmd(args);
		if (!result.containsKey("LatestVersion") || !result.containsKey("LatestURL"))
			throw new InvalidSysrootException("Invalid return value from ade");

		Sysroot version = new Sysroot(result.get("LatestVersion"));
		try {
			version.setUrl(new URL(result.get("LatestURL")));
		} catch (MalformedURLException e) {
			throw new InvalidSysrootException("Malformed URL");
		}
		return version;
	}
	
	public static Sysroot verify(File archive) throws InvalidSysrootException {
		List<String> args = new ArrayList<String>();

		args.add("verify");
		args.add("--file");
		args.add(archive.getAbsolutePath());

		return new Sysroot(runCmd(args).get("VerifiedVersion"));
	}
	
	public static Sysroot extract(File archive) throws InvalidSysrootException {
		List<String> args = new ArrayList<String>();

		args.add("install");
		args.add("--file");
		args.add(archive.getAbsolutePath());

		return new Sysroot(runCmd(args).get("InstalledVersion"));
	}
}
