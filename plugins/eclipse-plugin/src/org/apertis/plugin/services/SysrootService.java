/*Copyright © 2016 Collabora Ltd.

SPDX-License-Identifier: MPL-2.0
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apertis.plugin.ApertisPlugin;
import org.apertis.plugin.Constants;
import org.apertis.plugin.DownloadInterruptedException;
import org.apertis.plugin.InvalidSysrootException;
import org.apertis.plugin.Sysroot;
import org.apertis.plugin.SysrootManager;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.preference.IPreferenceStore;

public class SysrootService {

	public Sysroot getInstalledVersion() throws InvalidSysrootException {
		return SysrootManager.getInstalled();
	}

	public Sysroot getLatestVersion() throws InvalidSysrootException {
		return SysrootManager.getLatest();
	}

	public File downloadSysroot(Sysroot sysroot, IProgressMonitor monitor) throws IOException, DownloadInterruptedException {
		File homeDir = new File(System.getProperty("user.home"));
		File file = File.createTempFile("sysroot", ".tar.gz", homeDir);
		saveUrl(sysroot.getUrl(), file.getAbsolutePath(), monitor);

		return file;
	}

	public void verifyArchive(Sysroot sysroot, File archive, IProgressMonitor monitor) throws InvalidSysrootException {
		SysrootManager.verify(archive);
	}

	public void extractArchive(Sysroot sysroot, File archive, IProgressMonitor monitor) throws InvalidSysrootException {
		SysrootManager.extract(archive);
	}

	protected void setSSL() {
		IPreferenceStore store = ApertisPlugin.getDefault().getPreferenceStore();
		boolean checkSSL = store.getBoolean(Constants.APERTIS_SYSROOT_UPDATE_SSL_PREFERENCE);

		// disable SSL check if set in preferences
		if (checkSSL)
			return;
		
		/* FIXME: It seems not to be a good idea to enforce system property.
		 * It would be better to have 'Advanced Preferences' in UI so user is able to
		 * set properties.
		 * https://phabricator.apertis.org/T3299 */
		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(
					java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(
					java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			ApertisPlugin.getDefault().logError ("Failed to set a default ssl socket factory", e);
		}			
	}

	private void saveUrl(URL url, String filename, IProgressMonitor monitor) throws MalformedURLException, IOException, DownloadInterruptedException
	{
		URLConnection uc = null;
		BufferedInputStream in = null;
		FileOutputStream fout = null;
		int contentLength = -1;
		int currentChunk = 0;
		int totalChunks;
		int skipChunks;

		Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				IPreferenceStore store = ApertisPlugin.getDefault().getPreferenceStore();
				String username = store.getString(Constants.APERTIS_SYSROOT_UPDATE_USERNAME_PREFERENCE);
				String password = store.getString(Constants.APERTIS_SYSROOT_UPDATE_PASSWORD_PREFERENCE);
				return new PasswordAuthentication(username, password.toCharArray());
			}
		});

		setSSL();

		try
		{
			uc = url.openConnection();
			contentLength = uc.getContentLength();
			totalChunks = contentLength / 1024;

			// we can do 100 steps on the monitor progress bar, but we
			// have more than 100 chunks, then we need to skip several
			// chunks. this number is rounded down.
			skipChunks = totalChunks / 100;

			in = new BufferedInputStream(url.openStream());
			fout = new FileOutputStream(filename);

			byte data[] = new byte[1024];
			int count;
			while ((count = in.read(data, 0, 1024)) != -1)
			{
				if (monitor.isCanceled())
					throw new DownloadInterruptedException(); // monitor asked to cancel the download
				fout.write(data, 0, count);
				currentChunk++;
				if ((currentChunk % skipChunks) == 0)
				{
					monitor.worked(1);
				}
			}
			if (count != 1024) // missing bytes
				monitor.worked(1);
		}
		finally
		{
			if (in != null)
				in.close();
			if (fout != null)
				fout.close();
		}
	}
}
