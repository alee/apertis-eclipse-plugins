/*Copyright © 2016 Collabora Ltd.

SPDX-License-Identifier: MPL-2.0
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

package org.apertis.plugin;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sysroot implements Comparable<Sysroot> {
	private String version;
	private String distro;
	private String release;
	private String architecture;
	private String date;
	private Integer build;
	private URL url;
	
	public Sysroot(String version) throws InvalidSysrootException {
		Pattern p = Pattern.compile("(\\w*)-(\\d\\d\\.\\d\\d)-(\\w*)_(\\d{8})\\.(\\d+)");
		Matcher m = p.matcher(version);
		
		if (!m.matches())
			throw new InvalidSysrootException("Invalid version string format " + version);

		this.url = null;
		this.version = m.group(0);
		this.distro = m.group(1);
		this.release = m.group(2);
		this.architecture = m.group(3);
		this.date = m.group(4);
		this.build = Integer.decode(m.group(5));
	}
	
	public URL getUrl() {
		return this.url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}
	
	public String getId() {
		return this.distro + "-" + this.release + "-" + this.architecture;
	}
	
	@Override
	public String toString() {
		return this.version;
	}
	
	public boolean isCompatible(Sysroot other) {
		return this.distro.compareTo(other.distro) == 0 &&
			   this.release.compareTo(other.release) == 0 &&
			   this.architecture.compareTo(other.architecture) == 0;
	}

	@Override
	public int compareTo(Sysroot other) {
		if (!isCompatible(other))
			return -1;
		if (this.date.compareTo(other.date) != 0)
			return this.date.compareTo(other.date);
		if (this.build.compareTo(other.build) != 0)
			return this.build.compareTo(other.build);
		return 0;
	}

}
