/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin.jobs;

import java.io.OutputStream;

import org.apertis.plugin.ApertisPlugin;
import org.apertis.plugin.Constants;
import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.CommandLauncher;
import org.eclipse.cdt.core.resources.IConsole;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;

public class AppInstallJob extends Job {

	private String device;
	private IProject project;
	private boolean installation = true;

	public AppInstallJob(String device, IProject project) {
		super("RemoteInstallJob");
		this.device = device;
		this.project = project;
	}
	
	public AppInstallJob(String device, IProject project, boolean installation) {
		this (device, project);
		this.installation = installation;
	}

	public AppInstallJob(IProject project) {
		super("LocalInstallJob");
		this.device = null;
		this.project = project;
	}
	
	public AppInstallJob(IProject project, boolean installation) {
		this (project);
		this.installation = installation;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		IConsole console = CCorePlugin.getDefault().getConsole("org.apertis.plugin.ui.apertisConsole");
		CommandLauncher launcher = new CommandLauncher();
		IPath location = this.project.getLocation();
		String[] args = new String[3];
		MultiStatus result = null;

		if (monitor == null)
			monitor = new NullProgressMonitor();

		args[0] = "--format=parseable";
		if (this.installation) {
			args[1] = "install";
		} else {
			args[1] = "uninstall";	
		}
		if (this.device != null) {
			args[2] = "--device=" + this.device;
		} else {
			args[2] = "--simulator";
		}

		try {
			console.start(this.project);
			OutputStream stdout = console.getOutputStream();
			OutputStream stderr = stdout;

			launcher.execute(new Path("ade"), args, null, location, null);
			stdout.write(launcher.getCommandLine().getBytes());
			stdout.write('\n');
			if (launcher.waitAndRead(stdout, stderr, monitor) != CommandLauncher.OK) {
				throw new Exception(stderr.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = new MultiStatus(ApertisPlugin.getUniqueIdentifier(), IStatus.ERROR,
					Constants.INSTALL_ERROR_MESSAGE, e);
		}

		if (result == null) {
			result = new MultiStatus(
					ApertisPlugin.getUniqueIdentifier(),
					IStatus.OK,
					"",
					null);
		}

		return result;
	}

}
