/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin;

import org.apertis.plugin.jobs.AppInstallJob;
import org.apertis.plugin.jobs.AppLaunchJob;
import org.eclipse.cdt.debug.core.CDebugUtils;
import org.eclipse.cdt.debug.core.ICDTLaunchConfigurationConstants;
import org.eclipse.cdt.launch.AbstractCLaunchDelegate2;
import org.eclipse.cdt.launch.remote.IRemoteConnectionConfigurationConstants;
import org.eclipse.cdt.managedbuilder.core.IConfiguration;
import org.eclipse.cdt.managedbuilder.core.IManagedBuildInfo;
import org.eclipse.cdt.managedbuilder.core.ManagedBuildManager;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;

public class LocalLaunchDelegate extends AbstractCLaunchDelegate2 
{
	@Override
	protected String getPluginID() {
		return ApertisPlugin.getPluginId();
	}
	
	@Override
	public boolean buildForLaunch(ILaunchConfiguration configuration, String mode, IProgressMonitor monitor)
			throws CoreException {
		IProject project = configuration.getMappedResources()[0].getProject();
		IConfiguration config = ManagedBuildManager.getExtensionConfiguration("org.apertis.plugin.configuration.build.simulator");
		IManagedBuildInfo info = ManagedBuildManager.getBuildInfo(project);
		info.setDefaultConfiguration(config);

		return super.buildForLaunch(configuration, mode, monitor);
	}

	@Override
	public void launch(ILaunchConfiguration config, String mode, ILaunch launch, IProgressMonitor monitor)
			throws CoreException {
		assert mode.equals(ILaunchManager.RUN_MODE);
		IProject project = config.getMappedResources()[0].getProject();
		
		installToSimulator(config, project);
		executeInSimulator(config, project);
	}

	@SuppressWarnings("restriction")
	protected void installToSimulator(ILaunchConfiguration config, IProject project)
			throws CoreException {
		boolean skipDownload = config
				.getAttribute(
						IRemoteConnectionConfigurationConstants.ATTR_SKIP_DOWNLOAD_TO_TARGET,
						false);
		
		if (skipDownload)
			return;

		Job job = new AppInstallJob(project);
		job.schedule();
		
		try {
			job.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	protected void executeInSimulator(ILaunchConfiguration config, IProject project)
			throws CoreException {
		String arguments = config
				.getAttribute(
						ICDTLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS,
						(String) null);
		
		Job job = new AppLaunchJob(project, "", arguments);
		job.schedule();
		try {
			job.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
