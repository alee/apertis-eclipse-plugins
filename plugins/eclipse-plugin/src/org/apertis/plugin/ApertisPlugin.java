/*Copyright © 2016 Collabora Ltd.

SPDX-License-Identifier: MPL-2.0
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin;

import org.apertis.plugin.jobs.SysrootInstallJob;
import org.apertis.plugin.jobs.SysrootUpdateJob;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * The activator class controls the plug-in life cycle
 */
public class ApertisPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.apertis.plugin"; //$NON-NLS-1$

	public static final String ID_LAUNCH_CONFIGURATION = "org.apertis.plugin.remoteApplicationLaunchType"; //$NON-NLS-1$

	// The shared instance
	private static ApertisPlugin plugin;

	public ApertisPlugin() {
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static ApertisPlugin getDefault() {
		return plugin;
	}

	public static String getPluginId() {
		return PLUGIN_ID;
	}

	public static String getUniqueIdentifier() {
		if (getDefault() == null) {
			// If the default instance is not yet initialized,
			// return a static identifier. This identifier must
			// match the plugin id defined in plugin.xml
			return PLUGIN_ID;
		}
		return getDefault().getBundle().getSymbolicName();
	}

	public static <T> T getService(Class<T> service) {
		BundleContext context = plugin.getBundle().getBundleContext();
		ServiceReference<T> ref = context.getServiceReference(service);
		return ref != null ? context.getService(ref) : null;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	public void installSysroot(Sysroot sysroot) {
		SysrootInstallJob installJob = new SysrootInstallJob(
				Constants.SYSROOT_INSTALL_JOB_NAME, sysroot);
		installJob.schedule();
	}

	public void updateSysroot(Sysroot sysroot) {
		SysrootUpdateJob updateJob = new SysrootUpdateJob(
				Constants.SYSROOT_UPDATE_JOB_NAME, sysroot);
		updateJob.schedule();
	}

	public void logError(String message, Exception e) {
		IStatus status = new Status(Status.ERROR, PLUGIN_ID, message, e);
		getLog().log(status);
	}

	public void logInfo(String message) {
		IStatus status = new Status(Status.OK, PLUGIN_ID, message);
		getLog().log(status);
	}

}
