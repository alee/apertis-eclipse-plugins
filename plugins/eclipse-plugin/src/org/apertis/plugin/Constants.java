/*Copyright © 2016 Collabora Ltd.

SPDX-License-Identifier: MPL-2.0
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

package org.apertis.plugin;

public class Constants {

	// preference keys
	public static final String APERTIS_SYSROOT_UPDATE_URL_PREFERENCE = "updateUrl";
	public static final String APERTIS_SYSROOT_UPDATE_USERNAME_PREFERENCE = "updateUsername";
	public static final String APERTIS_SYSROOT_UPDATE_PASSWORD_PREFERENCE = "updatePassword";
	public static final String APERTIS_SYSROOT_UPDATE_SSL_PREFERENCE = "checkSSL";

	// default preferences
	public static final String DEFAULT_PREFIX_UPDATE_URL = "https://images.apertis.org/daily";
	public static final String DEFAULT_SUFFIX_UPDATE_URL = "/sysroot";
	public static final String DEFAULT_SYSROOT_PATH = "/opt/sysroot/apertis";
	public static final String APERTIS_DEVICE_COUNT_PREFERENCE = "deviceCount";
	public static final int DEFAULT_DEVICE_COUNT = 0;
	public static final String APERTIS_DEVICE_NAME_PREFERENCE = "deviceName";
	public static final String APERTIS_DEVICE_ADDRESS_PREFERENCE = "deviceAddress";
	public static final String APERTIS_DEVICE_USER_PREFERENCE = "deviceUsername";
	public static final String APERTIS_DEVICE_PASSWORD_PREFERENCE = "devicePassword";
	public static final String APERTIS_DEVICE_ARCH_PREFERENCE = "deviceArch";
	public static final String ARCH_X86 = "x86";
	public static final String ARCH_ARM = "ARM";
	
	//
	
	public static final String VERSION_FILE_PATH = "/binary/etc/image_version";
	public static final String UPDATE_SYSROOT_SCRIPT_PATH = "/usr/bin/sysroot_update.sh";
	public static final String SYNC_FILE_NAME = "image.rexpfd";
	public static final String APERTIS_VERSION_FILE_PATH = "/etc/image_version"; 
	//
	
	public static final String DEVICE_NAME_LABEL = "Name:";
	public static final String DEVICE_ADDRESS_LABEL = "Address:";
	public static final String DEVICE_USERNAME_LABEL = "Username:";
	public static final String DEVICE_PASSWORD_LABEL = "Password:";
	public static final String DEVICE_ARCH_LABEL = "Architecture:";
	public static final String ADD_DEVICE_TITLE = "Add a new device";
	public static final String MODIFY_DEVICE_TITLE = "Modify a device";
	public static final String ADD_DEVICE_SUBTITLE = "Enter the information for your device";
	public static final String ADD_BUTTON_LABEL = "Add";
	public static final String MODIFY_BUTTON_LABEL = "Modify";
	public static final String REMOVE_BUTTON_LABEL = "Remove";
	public static final String CHECK_UPDATE_BUTTON_LABEL = "Check for Update...";
	public static final String INSTALL_BUTTON_LABEL = "Install";
	public static final String UPDATE_BUTTON_LABEL = "Update";
	public static final String PENDING_LABEL = "Pending...";
	public static final String APERTIS_SYSROOT_PATH_PREFERENCE = "sysrootPath";
	public static final String APERTIS_SYSROOT_VERSION_PREFERENCE = "sysrootVersion";
	public static final String SYSROOT_PATH_LABEL = "Path:";
	public static final String SYSROOT_VERSION_LABEL = "Version:";
	public static final String MODIFY_SYSROOT_TITLE = "Modify a sysroot";
	public static final String CONFIRM_UPDATE_TITLE = "Confirm update";
	public static final String CONFIRM_UPDATE_MESSAGE = "A new version is available. Confirm to start updating";
	public static final String DOWNLOAD_TITLE = "Downloading file";
	public static final String DOWNLOAD_PROGRESS_MESSAGE = "The download is in progress. This dialog will close when it is finished";
	public static final String CHECK_UPDATE_SYSROOT_TITLE = "Check for Sysroot Update";
	public static final String UPDATE_SYSROOT_SUBTITLE = "Confirm to update selected sysroot to latest available version";
	public static final String SYSROOT_CURRENT_VERSION_LABEL = "Current version:";
	public static final String SYSROOT_AVAILABLE_VERSION_LABEL = "Latest available version:";
	public static final String SYSROOT_INSTALL_JOB_NAME = "Sysroot install";
	public static final String SYSROOT_UPDATE_JOB_NAME = "Sysroot update";
	public static final String UPDATE_URL_LABEL = "Address for update:";
	public static final String USERNAME_LABEL = "Username:";
	public static final String PASSWORD_LABEL = "Password:";
	public static final String SSL_CHECK_LABEL = "Check SSL certificate errors";
	public static final String INSTALL_SETUP_TITLE = "Install To Target Setup";
	public static final String INSTALL_SETUP_SUBTITLE = "Enter details for Install To Target configuration";
	public static final String INSTALL_PATH_LABEL = "Install path:";
	public static final String CONNECTION_LABEL = "Connection:";
	public static final String INSTALL_SETUP_MISSING_FIELDS = "All fields are required. Please complete the missing data.";

	// Jobs
	public static final String APERTIS_JOB_FAMILY = "ApertisJobs";
	public static final String SYSROOT_CHECK_JOB = "Checking for update";
	public static final String SYSROOT_CHECK_JOB_NAME = "Checking";
	public static final String SYSROOT_CHECK_INSTALLED_JOB = "Currently installed version";
	public static final String SYSROOT_CHECK_LATEST_JOB = "Latest available version";
	public static final String SYSROOT_INSTALL_JOB_BEGIN = "Starting sysroot update";
	public static final String SYSROOT_INSTALL_JOB_DOWNLOAD = "Downloading sysroot";
	public static final String SYSROOT_INSTALL_JOB_VERIFY = "Verifying downloaded archive";
	public static final String SYSROOT_INSTALL_JOB_EXTRACT = "Extracting new sysroot";
	public static final String SYSROOT_INSTALL_JOB_DONE = "Sysroot has been installed succesfully";
	public static final String SYSROOT_UPDATE_JOB_BEGIN = "Starting sysroot update";
	public static final String SYSROOT_UPDATE_JOB_DOWNLOAD = "Downloading sysroot";
	public static final String SYSROOT_UPDATE_JOB_VERIFY = "Verifying new sysroot";
	public static final String SYSROOT_UPDATE_JOB_EXTRACT = "Extracting new sysroot";
	public static final String SYSROOT_UPDATE_JOB_DONE = "Sysroot has been updated succesfully";
	public static final String SYSROOT_DOWNLOADED = "Sysroot downloaded succesfully";

	// Errors
	public static final int FILE_NOT_FOUND_ERROR = 100;
	public static final String FILE_NOT_FOUND_ERROR_MESSAGE = "File not found for the specified sysroot";
	public static final String UPDATE_ERROR_TITLE = "Error while updating";
	public static final int BAD_URL_ERROR = 101;
	public static final int IO_ERROR = 102;
	public static final String BAD_URL_ERROR_MESSAGE = "Malformed URL. Please check the address and try again";
	public static final String IO_ERROR_MESSAGE = "Error reading file. Please check the file is ok";
	public static final String DOWNLOAD_ERROR_TITLE = "Error while downloading";
	public static final String DOWNLOAD_ERROR_MESSAGE = "There was an error while downloading the file";
	public static final String UPDATE_VERSION_ERROR_MESSAGE = "Cannot obtain the version number from that path";
	public static final String UPDATE_VERSION_MISMATCH_ERROR_MESSAGE = "Mismatch between latest version and currenly installed one";
	public static final String VERSION_NOT_FOUND_MESSAGE = "Sysroot not found. Click OK to download";
	public static final String JOB_UPDATE_ERROR_MESSAGE = "There has been an error while executing the update";
	public static final String SYSROOT_NOT_INSTALLED = "Not installed";
	public static final String GENERATE_ERROR_MESSAGE = "Cannot run Apertis project configuration";
	public static final String NO_CONNECTION_ERROR_MESSAGE = "No remote connection specified";
	public static final String INVALID_CONNECTION_ERROR_MESSAGE = "Only SSH remote connections are supported";
	public static final String CANCELED_ERROR_MESSAGE = "Operation has been canceled";
	public static final String INSTALL_ERROR_MESSAGE = "Couldn't install application to target";
	public static final String DEBUG_ERROR_MESSAGE = "Couldn't start debugger on target";

	// Build
	public static final String BUILD_STOPPED = "Apertis project configuration has stopped";

	// Wizards
	public static final String ExportWizard_dialog_title = "Application Bundle";
	public static final String ExportWizard_dialog_message = "Export Apertis project to application bundle";
	public static final String ExportWizard_dialog_browse = "Browse";

	public enum ArchitectureE { X86, ARM }
}
